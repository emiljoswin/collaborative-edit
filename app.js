var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var d = require('./diff_match_patch');

var _dmp = new diff_match_patch();

app.get('/jquery-1.6.1.min.js', function (req, res) {
  res.sendfile('jquery-1.6.1.min.js');
});

app.get('/diff_match_patch.js', function (req, res) {
  res.sendfile('diff_match_patch.js');
});

app.get('/', function(req, res){
  console.log("inside /");
  res.sendfile('index.html');
});

var _prev = '';
var clients = ['a', 'b'];
io.on('connection', function (socket){
  socket.on('first', function (msg) {
    _prev = msg;
    console.log('first message', _prev);
    if (clients.length) {
      var cl = clients.shift();
      socket.emit('first-ack', cl);
    } else {
      socket.emit('first-ack', null);
    }
  });

  socket.on('chat msg', function(msg){
    //io.emit('chat msg', msg);
  });

  socket.on('diff', function (msg) {
    var cl = msg.id;
    var diffs = msg.diffs;
    console.log('diffs', diffs);
    console.log("prev", _prev);
    var patches = _dmp.patch_make(_prev, diffs);
    var result = _dmp.patch_apply(patches, _prev);
    console.log('result', result);
    _prev = result[0];
    // let the source accept the new string. Do not send to 
    // dest until source is not accepting
    // this would be a problem if the source is typing really fast
    // without a break.
    var other_cl = findOtherClient(cl);
    console.log('other client', other_cl);
    io.emit('merge', {id: other_cl, msg: _prev});
    //io.emit('diff-ack?', {id: other_cl, msg: _prev});
    //io.emit('merge', result[0]);
    //console.log('merge prev', _prev+'!');
  });

  /*
   * Thought of a three way handshake before merge.
   * But that technique is not necessary.
  socket.on('diff-ack', function (msg) {
    var id = msg.id;
    var msg = msg;
    var cl = findOtherClient (id);
    socket.emit('merge', {id: cl, msg:msg});
  });
  */
  socket.on('reset', function (msg) {
    var id = msg.id;
    console.log('reset call from', id, msg);
    clients = ['a', 'b'];
    io.emit('reset-ack', {from: id});
  });
});

function findOtherClient(id) {
  if (id == 'a') return 'b';
  else return 'a';
}

var port = 5000;
http.listen(process.env.PORT || port, function() {
  console.log('local instance listening on *:5000');
});
